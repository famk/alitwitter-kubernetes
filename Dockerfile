FROM ruby:2.6.5-alpine3.11 as builder

RUN apk add --update --no-cache \
    build-base \
    postgresql-dev \
    sqlite-dev \
    nodejs \
    yarn \
    tzdata

WORKDIR /alitwitter

COPY Gemfile* ./

RUN gem install -v "2.1.4" bundler

RUN bundle config --global frozen 1 \
 && bundle install -j4 --retry 3 \
 # Remove unneeded files (cached *.gem, *.o, *.c)
 && rm -rf /usr/local/bundle/cache/*.gem \
 && find /usr/local/bundle/gems/ -name "*.c" -delete \
 && find /usr/local/bundle/gems/ -name "*.o" -delete

COPY package.json yarn.lock ./

RUN apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers make python && \
  yarn install --quiet node-gyp -g &&\
  yarn install --quiet

# Copy the Rails app
COPY . ./

# Precompile assets
RUN RAILS_ENV=production SECRET_KEY_BASE=secret bundle exec rake assets:precompile

RUN apk del native-deps

# Remove folders not needed in resulting image
RUN rm -rf node_modules tmp/cache vendor/assets lib/assets spec

# Final stage
FROM ruby:2.6.5-alpine3.11

RUN apk add --update --no-cache \
    bash \
    sqlite-dev \
    postgresql-client \
    tzdata

RUN gem install bundler -v "2.1.4"

# Copy app with gems from former build stage
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=builder /alitwitter /alitwitter

# Set Rails env
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true
ENV EXECJS_RUNTIME Disabled
ENV RAILS_ENV production
ENV PATH /usr/local/bundle/bin:$PATH

WORKDIR /alitwitter

# Expose Puma port
EXPOSE 3000

# Save timestamp of image building
RUN date -u > BUILD_TIME

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
