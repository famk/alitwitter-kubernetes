# Ali Twitter kuberenetes
A simple twitter application made for Ali

As a user,
- I want to be able to create a tweet, so that I can express what is my mind.
- I want to be able delete a tweet, so that I can remove a tweet that I don't want to.
- I want to be able to see my tweets, so that I can read tweet I have created before.

## Prerequisites

- Kubernetes
- Helm 3
- Vagrant
- Virtualbox

## Introduction
This chart bootstraps a twitter application deployment on [Kubernetes](http://kubernetes.io) cluster using [Helm](https://helm.sh) package manager. This app use postgresql kubernetes cluster that is installed using helm.

## Start the minikube with virtualbox
```bash
[host]$ minikube start --driver=virtualbox
```
## Installing the Chart
To install this chart, run this command
```bash
[host]$ helm install alitwitter-release alitwitter-charts
```

Run this command from this project directory to deploys Gate on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameter that can be configured during installation.

## Uninstalling the Chart
To uninstall/delete the `alitwitter-release`:

```bash
[host]$ helm uninstall `alitwitter-release`
```

## Parameters
The following tables lists the configurable parameters of the Ali Twitter chart and their default values.

Parameter                           | Description                                               | Default
----------------------------------- | --------------------------------------------------------- | -------------
`replicaCount`                      | Node replicas (deployment)                                | `1`
`image.repository`                  | `alitwitter` image repository                             | `famkampm/alitwitter`
`image.tag`                         | `alitwitter` image tag                                    | `1.0-alpine`
`image.pullPolicy`                  | Image pull policy                                         | `Always`
`service.type`                      | Service type                                              | `NodePort`
`service.port`                      | Service port                                              | `80`
`service.nodePort`                  | Service nodePort                                          | `32766` 
`config.database.adapter`           | Database adapter for Ali Twitter App (PostgreSQL/SQLite3) | `postgresql`
`config.database.username`          | Database username                                         | `admin`
`config.database.password`          | Database password                                         | `password`
`config.database.ip`                | Database adapter host                                     | `postgres-prod-postgresql`
`config.database.port`              | Database adapter port                                     | `5432`
`config.database.development`       | Database name for development environment                 | `twitter`
`config.database.test`              | Database name for test environment                        | `twitter-test`
`config.database.production`        | Database name for production environment                  | `twitter`
`config.env`                        | The value for `RAILS_ENV`                                 | `production`
`config.secretKeyBase`              | The value for `SECRET_KEY_BASE`                           | `secret`

install using a chart folder that specifies all configuration that is needed to install the app.
```console
[host]$ helm install alitwitter-release alitwitter-charts
```
## Installing postgres using helm
you can install postgres using helm postgresql-charts/values.yaml for customized value that is suitable with alitwitter-chart config.
```console
[host]$ helm install postgres-prod-postgresql -f postgresql-charts/values.yaml bitnami/postgresql
```
## How to Run
1. Get the application URL by running these commands:
```bash
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services alitwitter-release)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT
```
2. Go to Browser and go to `http://$NODE_IP:$NODE_PORT/tweets`

## CI/CD
to enable gitlab ci cd, you can provision gitlab-runner vm using vagrant.  
```console
[host]$ vagrant up
```
after that, you have to install needed dependencies:

- ruby 2.6.5:
```console
$ sudo apt -y install software-properties-common
$ sudo apt-add-repository ppa:brightbox/ruby-ng
$ sudo apt update
$ sudo apt install ruby2.6
```

- bundler 2.1.4
```console
$ gem install bundler -v '2.1.4'
```

- gitlab-runner
```console
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
$ sudo apt-get install gitlab-runner
```
- [register gitlab-runner](https://docs.gitlab.com/runner/register/)

- install docker
```console
$ sudo apt-get update
$ sudo apt install docker.io
$ sudo systemctl start docker
$ sudo systemctl enable docker
```
- Login using your account to push image to dockerhub 
```console
$ sudo docker login -u "username" -p "password" docker.io
```
NOTE. you need to create repository in your dockerhub account and change the values in `alitwitter-charts/values.yaml` accordingly

- install kubectl
```console
$ sudo apt-get update && sudo apt-get install -y apt-transport-https
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
$ echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
$ sudo apt-get update
$ sudo apt-get install -y kubectl
```

- install helm:
```console
$ sudo snap install helm --classic
```
In order to access minikube from your vm. you need to copy the credential from the host to your vm.
```console
[host]$ scp ~/.kube/config vagrant@192.168.100.100:/home/vagrant/.kube
[host]$ scp ~/.minikube/ca.crt vagrant@192.168.100.100:/home/vagrant/.minikube/
[host]$ scp ~/.minikube/profiles/minikube/client.crt vagrant@192.168.100.100:/home/vagrant/.minikube/profiles/minikube
[host]$ scp ~/.minikube/profiles/minikube/client.key vagrant@192.168.100.100:/home/vagrant/.minikube/profiles/minikube
```
Then copy these certificate from user vagrant to user gitlab-runner path.
```console
[host]$ vagrant ssh gitlab-runner
$ sudo mkdir /home/gitlab-runner/.minikube
$ sudo mkdir /home/gitlab-runner/.minikube/profiles/minikube/
$ sudo cp -r /home/vagrant/.minikube /home/gitlab-runner/.minikube
$ sudo cp /home/vagrant/.kube/config /home/gitlab-runner/.kube/
```
